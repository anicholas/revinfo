﻿//
// (C) Copyright 2013 by Andrew Nicholas
//
// This file is part of RevInfo.
//
// RevInfo is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RevInfo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with RevInfo.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RevInfo
{
    public partial class Form1 : Form
    {
        public Form1(string s)
        {
            InitializeComponent();
            textBox1.Text = "...loading";
            this.Text = "Extended Revit File Info For: " + Path.GetFileName(s.Replace("\"",""));
            getBasicFileInfo(s);
        }

        private void getBasicFileInfo(string s)
        {
            System.Diagnostics.ProcessStartInfo sdpsinfo = new System.Diagnostics.ProcessStartInfo(@"7z.exe", @"-y e " +  s  + " BasicFileInfo");
            sdpsinfo.RedirectStandardOutput = true;
            sdpsinfo.UseShellExecute = false;
            sdpsinfo.UseShellExecute = false;
            sdpsinfo.CreateNoWindow = true;
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo = sdpsinfo;
            p.Start();
            p.WaitForExit();
            showInfo();
            File.Delete(@"BasicFileInfo");
        }

        private void showInfo()
        {
            int i = 0;
            string s = "";
            foreach (string line in File.ReadLines(@"BasicFileInfo"))
            {
                if(i > 0){
                    s += line.Replace("\0","");
                    s += System.Environment.NewLine;
                }
                i++;
            }
            textBox1.Text = s;
            textBox1.Select(0, 0);
        }

    }
}
